#! /bin/sh

#-------------------------------------------------------------------------------
# Licence :
# ----------
#
# Copyright 2020-2021 Alain BENEDETTI
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#-------------------------------------------------------------------------------
# Descriptions :
# ---------------
#   This script is:
#   - A documentation on how to set up a middlebox as of Ubuntu 20.04
#   - The main script for setting iptables rules for the middlebox
#
#
# Important :
# -----------
#    Credit to the initial work (author + comments)
#    https://www.howtoforge.com/how-to-set-up-a-tor-middlebox-routing-all-virtualbox-virtual-machine-traffic-over-the-tor-network
#
#    How-To on this page has several limitations, and is broken in 16.04
#    - it does not filter out some traffic (even adding the UDP filter)
#      Indeed, it let through protocols like ICMP (or obscure SCP) and
#      more importantly ipV6! (Not blocked here but at the
#      system configuration level for the vnet0 interface)
#    - it works out of luck as you had 2 deamons listening to the same
#      port (53) on the same interface (your vnet0): tor and dnsmasq!
#    - as Ubuntu already has a core dnsmasq with NetworkManager,
#      installing a new one as instructed fails in systemd
#      (your system still works, but the middlebox won't: no DHCP)
#
#    Since modern linux distribution come with Systemd, the scripts
#    on the original page needed some adaptations..
#
#    Also, thanks to systemd (!) as of Ubuntu 20.04 you do NOT need to install
#    ANY additional tools to run the middlebox. Systemd does it all (!),
#    bridge-utils and dnsmasq are not necessary anymore.
#
#    ... you obviously still need to install tor, and also a container like
#    commonly VirtualBox, or tools like firejail, that's the whole point of
#    the "middlebox".
#
#    Because there are now only 6 files to put in place, it becomes overkill
#    to do it with a script. Thus this main script describes all the process
#    of setting up the middlebox system on a 20.04 Ubuntu (Desktop).
#
#
# Documentation:
# -------------
#   What is this "middlebox" for?:
#   ------------------------------
#	"Middlebox" allows you to run VirtualBox where all traffic is routed
#	trhough TOR without having to have a separate router machine.
#	You can also use it to run containers like firejail and send traffic
#	of the container to TOR
#
#   Convention:
#   -----------
#	In the documentation, when we speak of the brige interface we set up
#	we call it 'vnet0' and assume it has 172.16.0.1 on the host and
#	a network of 172.16.0.0/24.
#	You should change things accordingly to your own configuration, if you
#	dislike the name 'vnet0' (or it is already in use), feel free to use
#	any other name for you bridge interface. Same for the IP addresses,
#	especially if they are already in use for another private network,
#	change them to you likings.
#
#	When you need to create a file, you'll see a section like this:
#	file: /etc/foo/bar.config
#	-----
#	Content of the file
#		named /etc/foo/bar.config
#	-----
#	This means that you need to create the file: /etc/foo/bar.config
#	And the content of the file is what is between the lines with: -----
#	You should remove the # and TAB at the begining of each lines to get
#	the actual content of the file.
#
#	The files are also provided on the GitLab, should you prefer that
#	method. They are provided with a comment saying where their must be
#	put (or suggested).
#
#	Look at what is inside the file. If you need to change vnet0 or the
#	network addresses, do it to match your configuration.
#
#	When commands are suggested, you'll see:
#	command: ls -l /run/systemd/network/
#       --------
#	-rw-r--r--  1 root root  32 mai   23 17:40 10-netplan-vnet0.netdev
#	-rw-r--r--  1 root root  81 mai   23 17:40 10-netplan-vnet0.network
#       --------
#	The command is on the first line after the :
#	The output (if any interesting output) is between the ------ lines.
#	When no (interesting) output, you have 2 consecutive lines of ------.
#
#   Netplan:
#   --------
#	IMHO, netplan is just a PITA and brings no added value but complexity!
#	But well, it's there, and we don't want to break the whole distro by
#	removing it.
#	You'll notice that on the Desktop, you have a single rule (01) which
#	only says: "everything is handled by Network-Manager". It is wisely
#	named: /etc/netplan/01-network-manager-all.yaml
#
#	That does not work for us, because our bridge needs to be handled by
#	systemd (remember, no need using bridge-utils anymore!).
#
#	So we create a (00) simple rule:
#	file: /etc/netplan/00-middlebox.yaml
#       -----
#	network:
#	  version: 2
#	  bridges:
#	    renderer: networkd
#	    vnet0:
#	      link-local: [ ]
#       -----
#
#	This rule will run BEFORE the default rule that is installed by the
#	distribution, and willcreate a minimal config for the vnet0 bridge.
#	Where Netplan is dumb, is that we need other rules from systemd, but
#	we cannot use Netplan for that because it does not go that far. Netplan
#	is for "basic things"... that are anyway simple enough you wouldn't
#	need it!..
#	If you apply:
#	command: sudo netplan apply
#       --------
#       --------
#	You would notice that netplan created 2 files:
#	command: ls -l /run/systemd/network/
#       --------
#	-rw-r--r--  1 root root  32 mai   23 17:40 10-netplan-vnet0.netdev
#	-rw-r--r--  1 root root  81 mai   23 17:40 10-netplan-vnet0.network
#       --------
#
#	The first one declares the "device" (vnet0) and is fine.
#	The second one is a minimal config for a bridge, but we need to
#	superseed it because we need more.
#
#   networkd:
#   ---------
#	This is the part of systemd that handles the network.
#	As you see, we didn't even speficy the address in netplan, because
#	anyway we need rules that netplan is too dumb to allow express.
#	So we are doing that all as a systemd .network config file.
#	This file needs to have higher priority than the one created by
#	netplan, so that the rules we need are on, and not what netplan said.
#	If we start our filename with 00 and store it in /etc/systemd/network
#	that will do the trick, our file wil have higher priority.
#
#	So we create
#	file: /etc/systemd/network/00-middlebox.network
#	-----
#	[Match]
#	Name=vnet0
#
#	[Network]
#	LinkLocalAddressing=no
#	Address=172.16.0.1/24
#	ConfigureWithoutCarrier=yes
#	DHCPServer=yes
#
#	[DHCPServer]
#	EmitDNS=yes
#	DNS=172.16.0.1
#       -----
#
#	In fact we needed systemd to also act as a DHCP SERVER (not client!)
#	on our bridge, and to serve the DNS as 172.16.0.1 (which is the ip
#	assigned to vnet0 on the host).
#
#	So as you see, systemd will take in charge creating and upping the
#	(empty) bridge and will serve DHCP + emit DNS on it, for all the
#	VirtualBoxes that will connect to vnet0.
#
#	Now you can either reboot, or reset the network (you must have
#	"applied" the netplan, see above)
#
#	command: sudo systemctl restart systemd-networkd
#       --------
#       --------
#	You can check with
#	command: ip a
#       --------
#	3: vnet0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UNKNOWN group default qlen 1000
#	    link/ether f2:63:29:c3:1e:34 brd ff:ff:ff:ff:ff:ff
#	    inet 172.16.0.1/24 brd 172.16.0.255 scope global vnet0
#	       valid_lft forever preferred_lft forever
#       --------
#
#   tor:
#   ----
#	So now you need to configure tor for the middlebox.
#	You will have to set some directives for DNS and traffic routing, and
#	some optional ones should you want to access .onion sites from your
#	middlebox.
#
#	Directives to add are:
#	file: /etc/tor/torrc
#	-----
#	DNSPort 172.16.0.1:9053
#	TransPort 172.16.0.1:9040
#
#	# The 2 directives above are for accessing .onion addresses
#	AutomapHostsOnResolve 1
#	VirtualAddrNetworkIPv4 10.128.0.0/10
#
#	# Then the rest of the torrc file...
#	-----
#
#	Adapt the first 2 diretives to your configuration. 9040 is the usual
#	port for tor transport, for DNS, use anything but NOT 53 (your normal
#	DNS port!).
#	These 2 directives are listening on the address of our vnet0, set
#	it accordingly if you had to change from the example: 172.16.0.1
#
#	For accessing onion routers, the Virtual IPV4 network can be anything
#	that is not already in use on your host. The more adresses the better,
#	that is why the example above uses /10.
#
#
#   filtering (iptables):
#   ---------------------
#	This is done by this script.
#	Basically the traffic from the middlebox can go three ways:
#	- forwarded to tor (stdandard)
#	- to your local host (optional)
#	- forwarded normally as non-tor traffic (optional)
#
#	The scripts expects the path of the configuration file as parameter.
#	This configuration file is a shell script snipet that should contain
#	at least the three variable with non empty values:
#
#	file: as passed in parameter 1 of the command
#	-----
#	IFACE='vnet0'
#	IP4='172.16.0.1'
#	NET4='172.16.0.0/24'
#	-----
#	As explained above, you can adapt the values to your configuration.
#	NET4 should be consistent with IP4 (meaning IP4 is part of NET4)
#
#	Optional: the configuration can also specify:
#	-----
#	DNSPORT=9053
#	TRANSPORT=9040
#	-----
#	This is optional, and if not specified the values are those above.
#	Make sure then your /etc/tor/torrc refers to those ports.
#
#	Optional feature: NON-TOR forwarding
#	------------------------------------
#	If you want your middlebox to reach some addresses without going through
#	TOR, you must specify additional two variables.
#	The use of that is for example to access a NAS on your local network
#	from the middlebox.
#	-----
#	NON_TOR_FORWARD='192.168.0.0/16 172.32.1.0/24'
#	HOST_GW='192.168.0.10'
#	-----
#	The first variable is a list of hosts or network to forward to without
#	going through TOR. Elements of the list are separated by a space.
#	The second is the host's gateway that should be used to forward,
#	for example the IP assigned to the ethernet device of the host.
#
#	If one of those variables is empty (or not provided) there will not
#	be any "non-tor" forwarding.
#
#	Optional feature: access to the host
#	------------------------------------
#	Same as forwarding, if you want to access some services directly on
#	the host, you must give an IP assigned to another host's device than
#	the bridge device. For instance, it can be the same the gateway used
#	above.
#	-----
#	NON_TOR_FORWARD='192.168.0.0/16 172.32.1.0/24'
#	HOST_GW='192.168.0.10'
#	-----
#	NON_TOR_INPUT="${HOST_GW}"
#	-----
#	Same as above, if this variable is not provided or is empty, the
#	middlebox will have no access to the host (apart from DHCP obviously).
#
#
#   Installation:
#   -------------
#	After creation/modification of the various files above, install
#	this file in (suggested):
#	file: /etc/middlebox/middlebox.sh
#	-----
#	==> This file (the one you are reading!)
#	-----
#	(You can obviouly adapt if you don't like the name!)
#	Create you configuration file in the same directory.
#	Here is an example of a complete configuration file:
#
#	file: /etc/middlebox/middlebox.conf
#	-----
#	IFACE='vnet0'
#	IP4='172.16.0.1'
#	NET4='172.16.0.0/24'
#	DNSPORT=9053
#	TRANSPORT=9040
#	NON_TOR_FORWARD='192.168.0.0/16 172.31.79.0/24'
#	HOST_GW='192.168.0.10'
#	NON_TOR_INPUT="${HOST_GW}"
#	-----
#	Adapt it to what you need. Only the first 3 lines are mandatory.
#
#
#   Don't forget!
#   -------------
#	If you want your VirtualBox guest to go through TOR, to set them on
#	the vnet0 bridge interface.
#	If you forget that, the default is to use NAT, and all traffic will
#	get out of the guest through your host without ever using TOR.
#	You can also switch interfaces with the same guest between NAT and
#	the bridge for TOR, but that could be a very bad idea with some
#	proprietary O.S. because they track you, and will have the history of
#	your "real" IPs even when you decide to go through TOR later.
#
#
#   Optional last step, the systemd service file:
#   ---------------------------------------------
#	Now for you middlebox filtering rules to be set at each boot of your
#	machine (if that is what you need, otherwise you can just run the
#	script when you want to use the middlebox) you can create:
#
#	file: /etc/systemd/system/middlebox.service 
#	-----
#	[Unit]
#	Description=Firewall rules for the TOR middlebox
#	After=network.target
#	Before=systemd-user-sessions.service
#
#	[Service]
#	Type=oneshot
#	RemainAfterExit=yes
#	ExecStart=/bin/sh /etc/middlebox/middlebox.sh /etc/middlebox/middlebox.conf
#
#	[Install]
#	WantedBy=multi-user.target
#	-----
#
#	Then enable it with:
#	command: sudo systemctl enable /etc/systemd/system/middlebox.service
#	-----
#	Created symlink /etc/systemd/system/multi-user.target.wants/middlebox.service → /etc/systemd/system/middlebox.service.
#	-----
#
# -------
#
# Tested : Ubuntu Xenial (20.04)
# ------
#
# Date : 2020.05.26
# -----
#
# Author : Alain BENEDETTI
# ------
#
# History :
#  2021.10.11 Avoid duplication of the POSTROUTING rule for non tor forwards
#-------------------------------------------------------------------------------


if [ -z "${1}" ]; then
	echo "No configuration file specified."
	exit 1
fi

if [ -e "${1}" ]; then
	if [ -f "${1}" ] && [ -r "${1}" ]; then
		echo "Using confiuration file: ${1}."
	else
		echo "${1} is not a file or is not readable."
		exit 1
	fi
else
	echo "${1} does not exist."
	exit 1
fi

. "${1}"

if [ -z "${IFACE}" ] || [ -z "${IP4}" ]  || [ -z "${NET4}" ]; then
	echo "All 3 variables IFACE, IP4 and NET4 should be defined and non empty."
	exit 1
fi

if [ -z "${DNSPORT}" ]; then
	DNSPORT=9053
fi

if [ -z "${TRANSPORT}" ]; then
	TRANSPORT=9040
fi


# Since now we have a much more restrictive firewall,
# we must explicitely accept DHCP, otherwise the guest won't get an IP!
# If you have only guests where you use static IP, you can comment out
# those rules to be even more restrictive.
	iptables -A INPUT  -i ${IFACE} -p udp --dport 67 -j ACCEPT
	iptables -A OUTPUT -o ${IFACE} -p udp -s ${IP4} --sport 67 -d ${NET4} --dport 68 -j ACCEPT

	# We redirect and accept DNS requests to the port where tor is listening for them
	iptables -t nat -A PREROUTING -i ${IFACE} -s ${NET4} -p udp --dport 53 -j REDIRECT --to-ports ${DNSPORT}
	iptables -A INPUT  -i ${IFACE} -s ${NET4} -d ${IP4} -p udp --dport ${DNSPORT} -j ACCEPT
	iptables -A OUTPUT -o ${IFACE} -d ${NET4} -s ${IP4} -p udp --sport ${DNSPORT} -j ACCEPT

	for NET in ${NON_TOR_INPUT}; do
		iptables -t nat -A PREROUTING  -i ${IFACE} -s ${NET4} -d ${NET} -j RETURN
		iptables -A INPUT -i ${IFACE} -s ${NET4} -d ${NET} -j ACCEPT
		iptables -A OUTPUT  -o ${IFACE} -d ${NET4} -s ${NET} -j ACCEPT
	done

	for NET in ${NON_TOR_FORWARD}; do
		iptables -t nat -A PREROUTING  -i ${IFACE} -s ${NET4} -d ${NET} -j RETURN
		iptables -A FORWARD -i ${IFACE} -s ${NET4} -d ${NET} -j ACCEPT
	done
	[ -n "${NON_TOR_FORWARD}" ] && iptables -t nat -A POSTROUTING -s ${NET4} -j SNAT --to-source ${HOST_GW}

	# We redirect and accept all other TCP traffic to TOR
	iptables -t nat -A PREROUTING -i ${IFACE} -s ${NET4} -p tcp --syn -j REDIRECT --to-ports ${TRANSPORT}
	iptables -A INPUT  -i ${IFACE} -s ${NET4} -p tcp --dport ${TRANSPORT} -j ACCEPT
	iptables -A OUTPUT -o ${IFACE} -d ${NET4} -p tcp --sport ${TRANSPORT} -j ACCEPT

# We drop ALL forwards and input (not accepted above) for any protocol
# Remember that rejecting UDP is not enough, there is also ICMP, etc...
# Our TOR traffic does not go there since it has been "captured" by the redirect,
# it will appear as exiting from the host default GW, not from our bridged interface.

	iptables  -A INPUT   -i ${IFACE} -s ${NET4} -j DROP
	iptables  -A OUTPUT  -o ${IFACE} -d ${NET4} -j DROP
	if [ -n "${NON_TOR_FORWARD}" ]; then
		sysctl -w net.ipv4.ip_forward=1
		sysctl -w net.ipv4.conf.${IFACE}.forwarding=1
		iptables  -A FORWARD -i ${IFACE} -s ${NET4} -j DROP
	  else
		sysctl -w net.ipv4.conf.${IFACE}.forwarding=0
	  fi


