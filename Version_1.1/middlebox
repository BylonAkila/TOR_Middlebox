#!/bin/sh
### BEGIN INIT INFO
# Provides:          middlebox filtering
# Required-Start:    $syslog $network
# Required-Stop:     $syslog
# Default-Start:     2 3 4 5
# Default-Stop:
# Short-Description: middlebox filtering
# Description:       iptables rules for the middlebox
### END INIT INFO


#-------------------------------------------------------------------------------
# Licence :
# ----------
#
# Copyright 2016 Alain BENEDETTI
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#-------------------------------------------------------------------------------
# Descriptions :
# ---------------
#   This script sets the appropriate iptables rules for the middlebox.
#   There are 2 main use-cases for the script:
#   -1) you have set your VMs to MAINLY run as a middlebox
#   -2) you have VMs that you would like to OCCASIONALLY run as middlebox.
#
# Use case 1)
#    It is recommended, in this use case, that you install the script in
#    your startup as described above. This will ensure that your VMs that are
#    designed for it will go through TOR without having to run the script
#    manually before launching the VM. Indeed, if you don't do as recommended
#    and forget to run the script manually, you will end up with a VM that
#    will NOT go through TOR: probably not what you want!
#    In this use case, I recommend to setup your VM with 2 network interfaces:
#    a regular one (for ex.: a NAT one) when you don't want to go through TOR
#    and the bridged one for the middlebox. Before launching the VM you can
#    select the appropriate interface.
#    If you have started the VM on the bridged interface dedicated to TOR,
#    without having to restart the VM you can switch on and off TOR by running
#    the script directly or through the service/systemctl commands.
#    Attention: in the case you have several running VM going through TOR,
#    flipping the switch is global to all those running VMs.
#
# Use case 2)
#    Here, you won't normally install the script as init.
#    Just use your VMs normally, and when you want to go through TOR run
#    the script. Same as above, you can switch on and off when the VM is
#    running.
#
# Important :
# -----------
#    Credit to the initial work (author + comments)
#    https://www.howtoforge.com/how-to-set-up-a-tor-middlebox-routing-all-virtualbox-virtual-machine-traffic-over-the-tor-network
#
#    How-To on this page has several limitations, and is broken in 16.04
#    - it does not filter out some traffic (even adding the UDP filter)
#      Indeed, it let through protocols like ICMP (or obscure SCP) and
#      more importantly ipV6! (Not blocked here but at the
#      system configuration level for the vnet0 interface)
#    - it works out of luck as you had 2 deamons listening to the same
#      port (53) on the same interface (your vnet0): tor and dnsmasq!
#    - as Ubuntu already has a core dnsmasq with NetworkManager,
#      installing a new one as instructed fails in systemd
#      (your system still works, but the middlebox won't: no DHCP)
#
#    I have also made adaptations for Ubuntu and namely to make it
#    compatible with systemd on top of the usual systemV/upstart init.
#    The script is also adapted for startup, atlhough the initial idea
#    of the How-To was probably the use case 2 described above.
#
# Usage (CLI) :
# -------------
#  sudo middlebox [start|stop]
#
# Installation:
# -------------
# With systemV,
# - copy (or link) the script to /etc/init.d then run
# sudo update-rc.d middlebox start 80 2 3 4 5 .
#
# With systemD,
# - copy the middlebox.service file to /etc/systemd/system
# - this file should contains lines with the right path to where you put
#   this script, for example:
#      ExecStart=/bin/sh /usr/local/sbin/middlebox.sh start
#      ExecStop=/bin/sh /usr/local/sbin/middlebox.sh stop
# - enable the service with
# sudo systemctl enable  middlebox
#
# Version : 1.1
# -------
#
# Tested : Ubuntu Xenial (16.04)
# ------
#
# Date : 2016-06-21
# -----
#
# Author : Alain BENEDETTI
# ------
#
# History :
# -------
# 1.1: fixes: one of the DNS rules was useless
#      Some tests for displaying messages were off
#      Added check on net.ipv4.ip_forward
#      Simplified the masquerading that is done only at ifup now.
#
#-------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
# HOW TO CUSTOMIZE (your own network parameters)
#
# We get customization values this way:
# -1) If the specified configuration file (see Script constants) exists, we
#     source it. All variables found there have precedence over method 2 below.
# -2) all not defined variables after that are guessed from other config files
#
#  The configuration file can define:
#  NON_TOR  : list of networks (separated by space) we don't want routed via TOR
#             ex. NON_TOR="192.168.1.0/24 10.8.0.0/16"
#             If variable is not present, ALL will be routed to TOR.
#  TransPort: Port where TOR is listening for TCP connections.
#  DNSPort:   Port where TOR is listening for DNS resolution (don't  pick 53!)
#             ex. TransPort=9040; DNSPort=60
#             If those 2 variables are missing, we will fetch them from
#             your TOR configuration at /etc/tor/torrc
#  IFACE    : The bridged interface on which your middlebox sends traffic
#  MB_SUBNET: Your middlebox subnet.
#             ex. MB_SUBNET='172.16.0.0/24'
#             If missing we fetch both from /etc/network/interfaces and
#             grab the address and netmask from the FIRST bridge we find
#             If you have several brigdes, either put the one we need first
#             or configure the subnet in the configuration file.
#
#  Except for NON_TOR which can be missing, it is a fatal error if any of the
#  other personalization variables remain empty after this process.
#
# Performance Tips:
#  -1) putting all variables in the configuration file is faster because we
#      don't need to read and interpret other files. The drawback is that
#      it can be incoherent with the parameters you used on the interface
#      or in tor, and not work at all.
#  -2) there is less risk of incoherence if you leave the parameters where
#      they belong, but you should put them closer to the top of the file
#      especially when the file is long. The sed used to find the parameters
#      will exit as soon as the parameter is found (ignoring the rest of
#      the file)



#-------------------------------------------------------------------------------
# Script constants
# You should change them only if your configuration does not match the paths

THIS_SCRIPT="$0"
CONFIG="/etc/middlebox.conf"
TORRC='/etc/tor/torrc'
IFCONF='/etc/network/interfaces'
LOCK_FILE='/tmp/.middlebox.lock'

# IS_INIT is 'Y' when we detect this script runs from init, otherwise is ''
# - detection for systemd: our parent is PID 1
# - detection for upstart: there is an UPSTART_JOB variable in environment
IS_INIT=''
if which systemctl >/dev/null; then
  [ $PPID -eq 1 ] && IS_INIT='Y'
else
  env | grep -q 'UPSTART_JOB' && IS_INIT='Y'
fi

# When IPV4 forward is not activated we don't need the forwarding rules
# nor the masquerade target, so we set the variable FORWARD for that.
if [ "$(sysctl -b net.ipv4.ip_forward)" = '1' ]; then
  FORWARD='Y';
else
  FORWARD='';
fi

#-------------------------------------------------------------------------------
# Script utility functions

# Converts ip/Mask to cidr
# Courtesy: https://forums.gentoo.org/viewtopic-t-888736-start-0.html
#

mask2cdr ()
{
   # Assumes there's no "255." after a non-255 byte in the mask
   local x=${1##*255.}
   set -- 0^^^128^192^224^240^248^252^254^ $(( (${#1} - ${#x})*2 )) ${x%%.*}
   x=${1%%$3*}
   echo $(( $2 + (${#x}/4) ))
}

# This function will output on the log when in the init/shutdown process
# (when our parent is the process 1) and on stdout otherwise.
error()
{
  if [ -n "${IS_INIT}" ]; then
    logger "$1"
  else
    printf "%s\n" "$1"
  fi
  exit 1
}


#-------------------------------------------------------------------------------
# Configuration variable initialization and global checks

[ -f "${CONFIG}" ] && . "${CONFIG}"

if [ -z "${TransPort}" ]; then
  TransPort="$(sed -n '/^\s*TransPort/{s/\s*#.*//;s/.*TransPort\s*//p;q}' "${TORRC}")"
  [ -z "${TransPort}" ] && error "middlebox: no value set for TransPort in ${CONFIG} or ${TORRC}"
fi
if [ -z "${DNSPort}" ]; then
  DNSPort="$(sed -n '/^\s*DNSPort/{s/\s*#.*//;s/.*DNSPort\s*//p;q}' "${TORRC}")"
  [ -z "${DNSPort}" ] && error "middlebox: no value set for DNSPort in ${CONFIG} or ${TORRC}"
fi

if [ -z "${IFACE}" ]; then
  IFACE="$(sed -n '/^\s*iface/, ${:a;N;$!ba;s/\n/,/g;s/iface\s*/\n/g;p}' "${IFCONF}" | sed -n '1d;/,\s*bridge_/{s/\s*inet.*//;p;q}')"
  [ -z "${IFACE}" ] && error "middlebox: no value set for IFACE in ${CONFIG} or ${IFCONF}"
fi
if [ -z "${MB_SUBNET}" ]; then
  ADDR="$(sed -n '/^\s*iface/, ${:a;N;$!ba;s/\n/,/g;s/iface/\n/g;p}' "${IFCONF}" | sed -n '1d;/,\s*bridge_/{s/.*address\s*\([0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\).*/\1/;p;q}')"
  MASK="$(sed -n '/^\s*iface/, ${:a;N;$!ba;s/\n/,/g;s/iface/\n/g;p}' "${IFCONF}" | sed -n '1d;/,\s*bridge_/{s/.*netmask\s*\([0-9]*\.[0-9]*\.[0-9]*\.[0-9]*\).*/\1/;p;q}')"
  if [ -z "${ADDR}" ] || [ -z "${MASK}" ]; then
     error "middlebox: no value set for MB_SUBNET in ${CONFIG} or ${IFCONF}"
  fi
  MB_SUBNET="${ADDR}/$(mask2cdr "${MASK}")"
fi

[ "$(whoami)" != 'root' ] && error "middlebox: this script must be run as root."

case $1 in
   ''|start|stop|status|ifup) ;;
   *)       error "middlebox usage: $0 [start|stop]"
            ;;
esac


#-------------------------------------------------------------------------------
# The real filtering work starts here

# We run the script under lock to avoid nasty mixup...
# ... just in case you run in twice at the same time
# ... or at startup if it races with ifup on interface
(
  flock -w 1 9 || error "middlebox: Could not acquire the lock!"

  # This tests if our rules are running, so that we don't apply them twice
  # in the case you run twice with start, or give errors if you stop twice.
  RULES_ON=''
  iptables -C INPUT -i "${IFACE}" -p udp --dport 67 -j ACCEPT 2>/dev/null && RULES_ON='Y'

  PARAM="$1"

  # It param is not set, we guess if it should be start or stop.
  if [ -z "${PARAM}" ]; then
    if [ "${RULES_ON}" = 'Y' ]; then
      PARAM='stop'
      [ -z "${IS_INIT}" ] && echo "middlebox: no param, assuming stop."
    else
      PARAM='start'
      [ -z "${IS_INIT}" ] && echo "middlebox: no param, assuming start."
    fi
  fi

  case "${PARAM}" in
    start)  [ "${RULES_ON}" = 'Y' ] && error "middlebox: start option received, but rules are already active"
            # We add filtering rules
            ACTION='-A'
            ;;
    stop)   # 'stop' is supposed useful only from command line
            # we don't really need to do anything when system is stopping!
            #  /TODO/ explore how to do the same with SystemV
            which systemctl >/dev/null && [ "$(systemctl is-system-running)" = 'stopping' ] && exit 0
            [ "${RULES_ON}" != 'Y' ] && error "middlebox: stop option received, but rules not active"
            # We delete existing rules
            ACTION='-D'
            ;;
    status) # This is only for upstart, systemd guesses status with RemainAfterExit=Yes
            if [ "${RULES_ON}" = 'Y' ]; then
              echo "middlebox is started."
            else
              echo "middlebox is stopped."
            fi
            exit 0
            ;;
    ifup)   # This option is 'secret' (not mentionned in usage) because it is
            # supposed to be used ONLY in the /etc/network/interfaces.
            # NOTE: we masquerade our subnet only here.
            # - When we have no rules (middlebox stopped) all traffic will
            #   be forwarded and correctly masqued: all is NON_TOR
            # - When we have rules (middlebox started) since we let pass only
            #   what is NON_TOR on the forward, we have the desired effect,
            #   although the rule could be unnecessary if NON_TOR is empty.
            [ "${FORWARD}" = 'Y' ] && iptables -t nat -A POSTROUTING -s "${MB_SUBNET}" -j MASQUERADE 2>/dev/null
            exit 0
            ;;
  esac

  # Since now we have a much more restrictive firewall,
  # we must explicitely accept DHCP, otherwise the guest won't get an IP!
  # If you have only guests where you use static IP, you can comment out
  # those rules to be even more restrictive.
  iptables "${ACTION}" INPUT  -i "${IFACE}" -p udp --dport 67 -j ACCEPT
  iptables "${ACTION}" OUTPUT -o "${IFACE}" -p udp -s "${ADDR}" --sport 67 -d "${MB_SUBNET}" --dport 68 -j ACCEPT


  for NET in ${NON_TOR}; do
    iptables -t nat "${ACTION}" PREROUTING  -i "${IFACE}" -s "${MB_SUBNET}" -d "${NET}" -j RETURN
    # /TODO/ if someone find a way to know wheter a given subnet/host
    #        need both forward and input or just one of them, it would
    #        be useful to keep the table as small as possible.
    if [ "${FORWARD}" = 'Y' ]; then
      iptables "${ACTION}" FORWARD -i "${IFACE}" -s "${MB_SUBNET}" -d "${NET}" -j ACCEPT
    fi
    iptables "${ACTION}" INPUT     -i "${IFACE}" -s "${MB_SUBNET}" -d "${NET}" -j ACCEPT
    iptables "${ACTION}" OUTPUT    -o "${IFACE}" -d "${MB_SUBNET}" -s "${NET}" -j ACCEPT
  done

  # We redirect and accept DNS requests to the port where tor is listening for them
  iptables -t nat "${ACTION}" PREROUTING -i "${IFACE}" -s "${MB_SUBNET}" -p udp --dport 53 -j REDIRECT --to-ports "${DNSPort}"
  iptables "${ACTION}" INPUT  -i "${IFACE}" -s "${MB_SUBNET}" -d "${ADDR}" -p udp --dport "${DNSPort}" -j ACCEPT
  iptables "${ACTION}" OUTPUT -o "${IFACE}" -d "${MB_SUBNET}" -s "${ADDR}" -p udp --sport "${DNSPort}" -j ACCEPT

  # We redirect and accept TCP traffic to TOR (except NON-TOR subnets above)
  iptables -t nat "${ACTION}" PREROUTING -i "${IFACE}" -s "${MB_SUBNET}" -p tcp --syn -j REDIRECT --to-ports "${TransPort}"
  iptables "${ACTION}" INPUT  -i "${IFACE}" -s "${MB_SUBNET}" -p tcp --dport "${TransPort}" -j ACCEPT
  iptables "${ACTION}" OUTPUT -o "${IFACE}" -d "${MB_SUBNET}" -p tcp --sport "${TransPort}" -j ACCEPT


  # We reject ALL forwards and input (not accepted above) for any protocol
  # Remember that rejecting UDP is not enough, there is also ICMP, etc...
  # Our TOR traffic does not go there since it has been "captured" by the redirect,
  # it will appear as exiting from the host default GW, not from our bridged interface.

  [ "${FORWARD}" = 'Y' ] && iptables  "${ACTION}" FORWARD -i "${IFACE}" -s "${MB_SUBNET}" -j REJECT --reject-with icmp-net-prohibited
  iptables  "${ACTION}" INPUT   -i "${IFACE}" -s "${MB_SUBNET}" -j REJECT --reject-with icmp-net-prohibited
  iptables  "${ACTION}" OUTPUT  -o "${IFACE}" -d "${MB_SUBNET}" -j REJECT --reject-with icmp-net-prohibited

  # Note, if you want to log what has been blocked from the guest, uncomment the LOG targets.
  # iptables  "${ACTION}" FORWARD -s "${MB_SUBNET}" -j LOG --log-level debug --log-prefix "MiddleBox Tor>>" --log-ip-options
  # iptables  "${ACTION}" INPUT   -s "${MB_SUBNET}" -j LOG --log-level debug --log-prefix "MiddleBox Tor>>" --log-ip-options

) 9>"${LOCK_FILE}"
