# TOR_Middlebox

**Purpose:** set up a TOR Middlebox to route all traffic from a VM to TOR

## **New Version 20.04:** compatible with Ubuntu 20.04 - May 2020
See files in the 20.04 directory.   
The main file is **middlebox.sh**, it has all the documentation and allows to create the rest of the configuration needed.   
For convenience, the other 5 files that are needed are also provided in the directory.   
   
Versions 2.0 and 1.1 will NOT work with 20.04, things have changed a lot!   

The Wiki has been updated with 20.04 instructions. As you will see, it's much simpler.   

## Older versions
**Version:** 2.0.0 - January 2018
Version 1.1 still under the Version 1.1 directory.

**Changelog:**
- The setup of the tor middlebox is now fully automatic. All your configuration is in one file (by default /etc/middlebox/middlebox.conf). The script will take care of the modifications of the relevant files of your system configuration. The files that are altered are backuped. So, if something goes wrong, you manually revert to the previous situation.
- As a consequence to that fully automated process:
  - You don't need to read (and understand!) the wiki to have the tor middlebox.
  - You don't need to apply manual modifications to system files.
  - Obviously, for educational purpose, you can still look at the wiki, or look at how the script does things.
- Support for Ubuntu 14.04 is dropped from the automatic script (see Version 1.1 if you need that with Ubuntu 14.04)
- Since we alter several system files, the script checks that your distribution is indeed Ubuntu 16.04. If you adapt it to another distribution, you can contribute the script modifications.
- Support of ipv6 (as per issue #1: https://github.com/Bylon/TOR_Middlebox/issues/1)
- Support of firejail as a lighter solution than running a full VM (as per issue #2 https://github.com/Bylon/TOR_Middlebox/issues/2)

**Installation:**
- Download the script (middlebox)
- Run it
- It will display instructions if something is missing, otherwise it will do all the necessary configuration.

**Un-installation:**
- Remove the script
- Manually copy the backuped configuration files to where they belong.
  - By default the backups are in /etc/middlebox/backup
  - Under this directory, you have the full path to where the file belongs. _Example: /etc/middlebox/backup/etc/network/interfaces_

**Options:**
- Run the script with -h or --help to see the options.

## Notes
### The state of ipv6

Although ipv6 have been there for almost two decades, it is still a "work in progress" in many places.

#### Tor

- Tor does NOT support biding on a link-local ipv6 address as of Ubuntu 16.04 (tor version 0.2.9). Even in the development version (0.3.4) as of this writing (January 2018) this is not implemented.
  - See ticket: https://trac.torproject.org/projects/tor/ticket/23819
  - I have developped a temporary workaround (see the ticket if you are curious)
  - For convenience, the script will pull the patched version of tor (should you need ipv6) from my ppa
  - Obviously, you can also patch the executable yourself if you wish to!

- Tor does not reliably return AAAA records when using DNS over ipv4, in spite all the indications in the configuration that we need ipv6.
  - See ticket: https://trac.torproject.org/projects/tor/ticket/24833
  - There is no workaround/patch for that so far.
  - That is quite unfortunate because it means that if you really need ipv6 inside your VM or inside firejail, the only way is to change your tor identity as long as you happen to hit an exit node that returns AAAA records.
  - Obviously the other working workaround is to input numerical ipv6 addresses. That always work since you then don't need DNS anymore, but it is very cumbersome.
  - When you configure with only a single ipv6 stack (and no ipv4), DNS resolution through tor goes over ipv6, and in this case you reliably get AAAA records (and A records too that you don't need since you don't have ipv4!)

#### Firejail

- This is small utility that uses the relatively new "user namespaces" in the kernel to provide a lightweight container to sandbox userland applications.
- firejail refuses to start if you do not provide an ipv4 stack! https://github.com/netblue30/firejail/issues/1721
- firejail does not provide an option for dns6.  https://github.com/netblue30/firejail/issues/1722

- firejail has other bugs unrelated to ipv6 that affect us:
  - When firejail is launched on a bridged interface, this interface is broken for VirtualBox. A working workaround is to down/up the interface, but to avoid that, the script will setup two different interfaces. By default you should run VirtualBox on vnet0 and firejail on vnet1. https://github.com/netblue30/firejail/issues/1669
  - When you are in the middlebox, there is no communication with the dbus used by your host to configure gnome/unity behaviours. That means that for example if you set the menu of the application to be in the title bar, an graphical application launched in the sandbox won't have that since it can't get the system parameter, and will use the default. https://github.com/netblue30/firejail/issues/1668


## Todo

Since I have other projects, and considering the poor state of progress of ipv6, I'll leave things as-is for the moment.
ipv6 works but is unreliable.

What could be done to improve the situation is:
- find a way to use DNS over ipv6 with tor since that works. It is obviously impossible with firejail due to the limitations describes above, but could possibly be done for VirtualBox since if works fine with a single ipv6. What I didn't manage to do here (not sure it is possible) is make DHCP (ipv4) return all but DNS, and provide the DNS with radvd (which works).

- dnsmasq, that is already there and working with 16.04 is supposed to do also router advertisments (RA) for ipv6. Not sure that is due to the fact that it is launched by NetworkManager with special options, but I never managed to make it work for this RA. We are using radvd instead. But if someone knows how to get the RA we need for our configuration through dnsmasq, that would relieve us of having a install radvd.
